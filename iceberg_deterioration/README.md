# final_project
Directory for AOS 573 Final Project on iceberg deterioration in the Southern Ocean, including:
- The project notebook (iceberg_deterioration.ipynb) with all analysis, text, and figures
- adaptor.mars.internal-1701378082.8828773-16386-12-ea35f0ea-00bd-4c3f-94c0-e53af78c29eb.nc for ERA5 data
- mwb62*.nc for the wave buoy data
- (OSCAR netCDF files are in a separate location)
- The environment .yml file
